var fs = require('fs');

var glob = require("glob");

module.exports = function(sourceFolder, outputFile) {
	glob(sourceFolder+ "/**/*.json", null, function(err, files) {
		var configObj = {};
		for (var i =0; i < files.length; i++) {
			var file = files[i];

			var contents = fs.readFileSync(file, 'utf8');
			var parsedObj = JSON.parse(contents);


			var cleanedPath = file.substring(sourceFolder.length, file.length);
			if(cleanedPath[0] == "/") {
				cleanedPath = cleanedPath.substring(1, cleanedPath.length);
			}

			cleanedPath = cleanedPath.substring(0, cleanedPath.length - 5);

			var pathList = cleanedPath.split("/");

			var currentPath = configObj;
			for (var j = 0; j < pathList.length; j++) {
				if(typeof (currentPath[pathList[j]]) === 'undefined') {
					currentPath[pathList[j]] = {};
				}
				if(j == pathList.length - 1) {
					currentPath[pathList[j]] = parsedObj;
				}
				currentPath = currentPath[pathList[j]]
			}

		}
		
		var result = JSON.stringify(configObj, null, 4);

		fs.writeFileSync(outputFile, result, 'utf8');
	});
}

